# chronicle_test



## Setup

1. create local environment
```
python -m venv env
```
2. upgrade local environment
```
python -m pip install --upgrade pip
```
3. install requirements
```
pip install -r requirements.txt
```
4. database migration
```
cd OfflineTest
python manage.py makemigrations
python manage.py migrate
```
5. run project
```
python manage.py runserver
```
6. the API
access the API with http://localhost:8000/api/
