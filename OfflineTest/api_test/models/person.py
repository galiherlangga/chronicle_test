from django.db import models

# Create your models here.
class Person(models.Model):
    fName       = models.CharField(max_length=100)
    lName       = models.CharField(max_length=100)
    isActive    = models.BooleanField()
    createdAt   = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.fName+" "+self.lName