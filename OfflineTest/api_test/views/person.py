from rest_framework import viewsets, status
from api_test.models import Person
from api_test.serializers import PersonSerializer

# Create your views here.
class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer