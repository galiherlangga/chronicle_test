from django.urls import path, include
from rest_framework import routers
from api_test import views

router = routers.DefaultRouter()
router.register(r'', views.PersonViewSet)

urlpatterns = [
    path(r'api/', include(router.urls))
]
